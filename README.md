## Browser Compatiblity/UI Testing 

### Information

Recent struggle with finding a docker image for Selenium that supports headless versions for both Firefox and Chrome, 
led to the process of building my own version.

The image is build with the following dependencies:
- latest Chrome and chromedriver
- latest Firefox and geckodriver
- latest stable PhantomJS webkit (v2.1.1)
- Selenium
- Python 3
- Xvfb and the python wrapper - pyvirtualdisplay


### Running:

- docker-compose

    ```
    docker-compose stop && docker-compose build && docker-compose up -d
    ```

Detailed examples on how to use Firefox with custom profile, Google Chrome with desired options or PhantomJS can be found in the source.
