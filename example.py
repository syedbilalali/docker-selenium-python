import os
import logging
import platform
import time
from pyvirtualdisplay import Display
from selenium import webdriver
from Screenshot import Screenshot_Clipping
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

ob=Screenshot_Clipping.Screenshot()

logging.getLogger().setLevel(logging.INFO)

BASE_URL_HOME = 'https://www.thefryecompany.com'
BASE_URL = 'https://www.thefryecompany.com/womens'
BASE_URL_MEN = 'https://www.thefryecompany.com/mens'
BASE_URL1 = '/data/womens.png'
BASE_URL_PLP = 'https://www.thefryecompany.com/mens/accessories'
BASE_URL_PDP = 'https://www.thefryecompany.com/mens/accessories/passport-case-d-420397'
BASE_URL_CHK = 'https://www.thefryecompany.com/checkout/cart/'



def chrome_example():
    display = Display(visible=0, size=(800, 600))
    display.start()
    logging.info('Initialized virtual display..')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument("disable-infobars")
    chrome_options.add_argument("--disable-extensions")  # disabling extensions
    chrome_options.add_argument('--start-maximized')
    chrome_options.add_argument("--disable-notifications")
    chrome_options.add_argument("--disable-dev-shm-usage")

    d = DesiredCapabilities.CHROME
    d['loggingPrefs'] = { 'browser':'ALL' }

    chrome_options.add_experimental_option('prefs', {
        'download.default_directory': os.getcwd(),
        'download.prompt_for_download': False,
    })
    logging.info('Prepared chrome options..')

    browser = webdriver.Chrome(chrome_options=chrome_options,desired_capabilities=d)
    logging.info('Initialized chrome browser..')

    browser.get(BASE_URL)
    time.sleep(5)

    logging.info('Accessed %s ..', BASE_URL)
    try:
        browser.find_element_by_xpath('//*[@id="bfx-wm-close-button"]').click()

    except NoSuchElementException:
        Q=0

    try:
        browser.find_element_by_xpath('/html/body/div[6]/aside[2]/div[2]/header/button').click()
    except NoSuchElementException:
        Q=0

    img_url=ob.Full_Scrrenshot(browser,r'/data/','womens.png')
    logging.info('Womens Category Screenshot Taken %s ..', BASE_URL)

    #with open("/data/errors.log", "a") as logfile:
    #    logfile.write('\n')
    #    logfile.write('""""""""""""""""""""""""WomensPage"""""""""""""""""""""""""')
    #for entry in browser.get_log('browser'):
    #    if entry['level'] != 'INFO':
    #        with open("/data/errors.log", "a") as logfile:
    #            logfile.write('\n')
    #            logfile.write(entry['message'])



    browser.get(BASE_URL_HOME)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','home.png')

    logging.info('Hompage Screenshot Taken %s ..', BASE_URL_HOME)

    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""HomePage"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])


    browser.get(BASE_URL_MEN)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','men.png')

    logging.info('Hompage Screenshot Taken %s ..', BASE_URL_MEN)



    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""MensPage"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])


    browser.get(BASE_URL)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','womens.png')

    logging.info('Womenpage Screenshot Taken %s ..', BASE_URL)

    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""WomensPage"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])


    browser.get(BASE_URL_PLP)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','PLP.png')

    logging.info('PLP Page Screenshot Taken %s ..', BASE_URL_PLP)

    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""PLP Page"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])

    browser.get(BASE_URL_PDP)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','PDP.png')

    logging.info('PDP Page Screenshot Taken %s ..', BASE_URL_PDP)

    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""PDP Page"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])

    browser.get(BASE_URL_CHK)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','CHKout.png')

    logging.info('PDP Page Screenshot Taken %s ..', BASE_URL_CHK)

    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""Checkout Page"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])


    logging.info('Page title: %s', browser.title)
    browser.quit()
    display.stop()



if __name__ == '__main__':
    chrome_example()

